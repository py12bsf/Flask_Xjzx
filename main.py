from flask_script import Manager
from flask_migrate import  Migrate, MigrateCommand

from app import create_project, db

if __name__ == '__main__':
    app = create_project("development")
    manager = Manager(app)
    migrate = Migrate(app, db)
    manager.add_command("db", MigrateCommand)

    # app.run()
    manager.run()




