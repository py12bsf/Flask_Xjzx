class Config(object):
    SECRET_KEY = "1"
    # 关闭数据的追踪
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:python@localhost:3306/flask_xjzx?charset=utf8'


class ProductionConfig(Config):
    DEBUG = False


CONFIG = {
    "development": DevelopmentConfig,
    "production": ProductionConfig
}
















