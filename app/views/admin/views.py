from app import db
from app.models.models import User, News
from . import admin_blu
from flask import render_template


@admin_blu.route("/index")
def index():
    print("后台展示")
    return render_template("admin/index.html")


@admin_blu.route("/user_count")
def user_count():

    return render_template("admin/user_count.html")


@admin_blu.route("/user_list")
def user_list():
    user_list = db.session.query(User).all()
    return render_template("admin/user_list.html", user_list=user_list)
#
#
@admin_blu.route("/news_review")
def new_review():
    new_list = db.session.query(News).order_by(News.create_time.desc()).limit(6)
    return render_template("admin/news_review.html", new_list=new_list)


@admin_blu.route("/news_review_detail/<new_id>")
def news_detail(new_id):
    new = db.session.query(News).filter(News.id == new_id).first()
    return render_template("admin/news_edit_detail.html", new=new)
#
#
@admin_blu.route("/news_edit")
def new_edit():
    return render_template("admin/news_edit.html")


@admin_blu.route("/news_type")
def new_type():
    return render_template("admin/news_type.html")






