from flask import request, session, jsonify

from app import db
from app.models.models import User, Collection
from . import news_blu


@news_blu.route("/collect", methods= ["GET", "POST"])
def collect():
    if request.method == "POST":

        news_id = request.json.get("news_id")
        action = request.json.get("action")
        mobile = session.get("mobile")
        user = db.session.query(User).filter(User.mobile == mobile).first()

        if action == "do":
            # 创建收藏对象
            obj = Collection(user_id=user.id, news_id=news_id)
            db.session.add(obj)
            db.session.commit()

            ret = {
                "errno": "0",
                "errmsg": "收藏成功"
            }

        elif action == "undo":
            obj = db.session.query(Collection).filter(Collection.user_id == user.id, Collection.news_id == news_id).first()
            db.session.delete(obj)
            db.session.commit()
            ret = {
                "errno": "0",
                "errmsg": "取消收藏成功"
            }

        else:
            ret = {
                "errno":"1001",
                "errmsg":"操作失败"
            }
        return jsonify(ret)












