
from . import index_blu
from flask import render_template, request, jsonify, session

from app import db
# 导入模型类
from app.models.models import News, Category, User, Follow


@index_blu.route("/")
@index_blu.route("/index")
def index():
    # 判断用户是否登录
    mobile = session.get("mobile")
    user_id = session.get("user_id")
    print("项目主页面展示","===",mobile)

    # 新闻排行
    obj = db.session.query(News).order_by(News.clicks.desc()).limit(6)

    if mobile:
        # 查询用户信息
        user = db.session.query(User).filter(User.mobile == mobile).first()
        print(user)
        return render_template("index/index.html", obj=obj, user= user)

    else:

        return render_template("index/index.html", obj=obj, user=mobile)

# http://127.0.0.1:5000/newslist?page=1&cid=0&per_page=10


@index_blu.route("/newslist")
def newslist():
    # 提取数据
    page = request.args.get("page", 1)
    cid = request.args.get("cid", 0)
    per_page = request.args.get("per_page", 10)

    page = int(page) if page.isalnum() else 1
    cid = int(cid) if cid.isalnum() else 0
    per_page = int(per_page) if per_page.isalnum() else 10

    # 从数据库中取出数据
    if cid == 0:
        paginate = db.session.query(News).order_by(News.create_time.desc()).paginate(page, per_page, False)
    else:
        paginate = db.session.query(News).filter(News.category_id == cid).paginate(page, per_page, False)
    news_list = paginate.items

    # 将查询出来的数据模型对象转化为需要的字符
    ret = {
        "totalPage": paginate.pages,
        "newsList": [x.to_dict() for x in news_list]

    }


    return jsonify(ret)


# http://127.0.0.1:5000/detail/1157
@index_blu.route("/detail/<news_id>")
def Detail(news_id):
    # 新闻排行
    obj = db.session.query(News).order_by(News.clicks.desc()).limit(6)
    # 新闻详情
    news = db.session.query(News).filter(News.id == news_id).first()

    # 获取新闻作者信息
    news_author = news.user
    news_num = len(news_author.news)


    # 判断用户是否登录
    mobile = session.get("mobile")
    user = db.session.query(User).filter(User.mobile == mobile).first()

    # 查看新闻作者粉丝数
    news_author.followers_num =  news_author.followers.count()

    # 判断用户是否能关注
    if user:
        can_follow = db.session.query(Follow).filter(Follow.followed_id==news.user_id, Follow.follower_id==user.id).first()
    else:
        can_follow = None

    # 判断用户是否能收藏
    new_id_list = []
    for a in user.collection_news:
        new_id_list.append(a.id)

    if int(news_id) not in new_id_list:
        can_collection = True
    else:
        can_collection = None

    return render_template("index/detail.html",
                           news=news, obj=obj,
                           news_author=news_author,
                           news_num=news_num,
                           user=user,
                           can_follow=can_follow,
                           can_collection=can_collection
                           )













