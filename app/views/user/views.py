import hashlib
import os

from app import db
from app.models.models import User, Follow, Category, News
from . import user_blu
from flask import render_template, request, session, jsonify, current_app


@user_blu.route("/")
def user_index():
    print("调用用户个人中心")
    mobile = session.get("mobile")
    user = db.session.query(User).filter(User.mobile == mobile).first()

    return render_template("index/user.html", user=user)


@user_blu.route("/user_base_info", methods=["GET", "POST"])
def user_base_info():
    if request.method == "GET":
        mobile = session.get("mobile")
        user = db.session.query(User).filter(User.mobile == mobile).first()

        return render_template("index/user_base_info.html", user=user)
    elif request.method == "POST":
        # {signature: "我的性格千万变", nick_name: "了不起的威猛", gender: "on"}
        # gender: "on"
        # nick_name: "了不起的威猛"
        # signature: "我的性格千万变"
        new_nick_name = request.json.get("nick_name")
        gender = request.json.get("gender")
        signature = request.json.get("signature")

        if not all([new_nick_name, gender, signature]):
            ret = {
                "errno":1001,
                "errmsg":"缺少必要参数"
            }
        else:
            # 获取User对象
            # try:
            mobile = session.get("mobile")
            user = db.session.query(User).filter(User.mobile == mobile).first()

            user.nick_name = new_nick_name
            user.signature = signature
            if gender == "MAN":
                user.gender = "MAN"
            elif gender == "WOMAN":
                user.gender = "WOMAN"
            db.session.commit()

            ret = {
                "errno":0,
                "errmsg":"修改成功！"
            }
            # except:
            #
            #     ret = {
            #         "errno":1002,
            #         "errmsg":"修改失败！"
            #     }
            #     db.session.rollback()

        return jsonify(ret)
        # request.json.get("")


@user_blu.route("/user_pic_info", methods = ["GET", "POST"])
def user_pic_info():

    if request.method == "GET":
        mobile = session.get("mobile")
        print(mobile)
        obj = db.session.query(User).filter(User.mobile == mobile).first()
        user = {
            "avatar_url":obj.avatar_url
        }
        return render_template("index/user_pic_info.html", user= user)
    elif request.method == "POST":
        avatar_image = request.files.get("avatar")
        if avatar_image:
            # 将图片后缀取出
            image_type = avatar_image.filename[avatar_image.filename.rfind("."):]
            name_hash = hashlib.md5()
            name_hash.update(avatar_image.filename.encode("utf-8"))
            print(name_hash)
            image_file_name = name_hash.hexdigest()
            avatar_image1 = image_file_name + image_type
            local_file_path = os.path.join("/static/upload/images", avatar_image1)
            file_path = os.path.join(current_app.root_path, "static/upload/images", avatar_image1)
            # print(current_app.root_path)
            # print(file_path)
            avatar_image.save(file_path)

            mobile = session.get("mobile")
            print(mobile)
            user = db.session.query(User).filter(User.mobile == mobile).first()
            user.avatar_url = local_file_path
            db.session.commit()

            ret = {
                "errno":0,
                "errmsg":"上传头像成功",
                "avatar_url":local_file_path
            }
        else:
            ret = {
                "errno":1001,
                "errmsg":"请选择上传头像"
            }


        return jsonify(ret)
        # image_type = f.filename[f.filename.rfind("."):]
        # name_hash = hashlib.md5()
        # name_hash.update(f.filename.encode("utf-8"))
        # image_file_name = name_hash.hexdigest()
        # image_file = image_file_name + image_type
        # print(image_file)
        # image_url = Send_qiniu(f)
        # print(image_url)


@user_blu.route("/user_follow", methods=["GET", "POST"])
def user_follow():
    if request.method == "GET":
        # 获取个人关注列表
        mobile = session.get("mobile")
        obj = db.session.query(User).filter(User.mobile == mobile).first()

        page = request.args.get("page", 1)

        paginate = obj.followed.paginate(int(page), 2, False)
        follower_list = [x.to_basic_info() for x in paginate.items]
        print(follower_list)

        return render_template("index/user_follow.html", follower_list=follower_list, obj=obj)
    elif request.method == "POST":
        # var params = {
        #     "user_id": user_id,
        #     "action": action
        # };
        user_id = request.json.get("user_id")
        author_id = request.json.get("author_id")
        action = request.json.get("action")

        print(user_id, author_id, action)
        if all([user_id, author_id, action]):
            if action == "undo":
                # 取消关注
                obj1 = db.session.query(Follow).filter(Follow.followed_id == author_id,
                                                             Follow.follower_id == user_id).first()
                print(obj1)
                db.session.delete(obj1)
                db.session.commit()
                print("取消关注")
            elif action == "do":

                obj1 = Follow(followed_id=author_id, follower_id=user_id)
                db.session.add(obj1)
                db.session.commit()
                print("添加关注")

            ret = {
                "errno": "0",
                "errmsg": "操作成功！"
            }
        else:
            ret = {
                "errno": "3002",
                "errmsg":"缺少必要参数！"
            }

        return jsonify(ret)


@user_blu.route("/user_pass_info")
def user_pass_info():
    return render_template("index/user_pass_info.html")


@user_blu.route("/user_collection")
def user_collection():
    mobile = session.get("mobile")
    user = db.session.query(User).filter(User.mobile == mobile).first()

    news_list = user.collection_news

    return render_template("index/user_collection.html", news_list=news_list)


@user_blu.route("/user_news_release", methods=["GET", "POST"])
def user_news_release():
    mobile = session.get("mobile")
    user = db.session.query(User).filter(User.mobile == mobile).first()
    if request.method == "GET":
        category = db.session.query(Category).all()
        return render_template("index/user_news_release.html", category=category)
    elif request.method == "POST":
        title = request.form.get("title")
        category = request.form.get("category")
        digest = request.form.get("digest")
        content = request.form.get("content")
        if not all([title, category, digest, content]):
            ret = {
                "errno":"1001",
                "errmsg": "缺少参数"
            }
            return jsonify(ret)

        new_image = request.files.get("index_image")
        if new_image:

            # 将图片后缀取出
            image_type = new_image.filename[new_image.filename.rfind("."):]
            name_hash = hashlib.md5()
            name_hash.update(new_image.filename.encode("utf-8"))
            print(name_hash)
            image_file_name = name_hash.hexdigest()
            avatar_image1 = image_file_name + image_type
            local_file_path = os.path.join("/static/upload/images", avatar_image1)
            file_path = os.path.join(current_app.root_path, "static/upload/images", avatar_image1)
            new_image.save(file_path)

            mobile = session.get("mobile")
            print(mobile)

            obj = News(title=title, digest=digest, content=content, category_id=category, user_id=user.id,
                       index_image_url=local_file_path, source=user.nick_name)

        else:
            obj = News(title=title, digest=digest, content=content, category_id=category, user_id=user.id, source=user.nick_name)

        db.session.add(obj)
        db.session.commit()

        ret = {
            "errno": "0",
            "errmsg": "发布新闻成功"
        }

        return jsonify(ret)


@user_blu.route("/user_news_list")
def user_news_list():
    return render_template("index/user_news_list.html")


# /user/release
# @user_blu.route("/release", methods=["GET", "POST"])
# def release():
#     print("调用发布新闻视图")
#     content = request.form.get("content")
#     print(content)
#     print(request.json)
#     return "ok"