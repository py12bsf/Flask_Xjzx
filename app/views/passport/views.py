from io import BytesIO

from flask import request, make_response, session, jsonify

from app import db
from app.models.models import User
from . import passport_blu


# http://127.0.0.1:5000/passport/register


# {mobile: "123", image_code: "123", password: "123123123"}
# image_code: "123"
# mobile: "123"
# password: "123123123"


@passport_blu.route("/register", methods = ["GET", "POST"])
def register():
    image_code = request.json.get("image_code")
    mobile = request.json.get("mobile")
    password = request.json.get("password")

    print(image_code, mobile, password)
    sum_code = session["img"]
    print("验证码对比", sum_code, "---", image_code)

    sum_code = sum_code.lower()
    image_code = image_code.lower()


    if sum_code != image_code:
        ret = {
            "errno":1001,
            "errmsg":"验证码错误"
        }

    else:
        ret = {
            "errno":0,
            "errmsg":"注册成功"
        }
        user = User()
        user.nick_name = mobile
        user.password_hash = password
        user.mobile = mobile
        try:
            db.session.add(user)
            db.session.commit()
            session["mobile"] = mobile
            session["user_id"] = user.id
        except Exception as ret:
            db.session.rollback()



    return jsonify(ret)


# http://127.0.0.1:5000/passport/image_code?code_id=27813b5d-7563-4e18-b9ed-06b570ac396b
@passport_blu.route("/image_code")
def image_code():
    # 导入自动生成验证码图片包
    from app.utils.get_code_image.get_code_image import create_code
    #
    code_str, code_img = create_code()

    buf = BytesIO()
    code_img.save(buf, "jpeg")
    image_data = buf.getvalue()
    response = make_response(image_data)
    response.headers['Content-Type'] = 'image/jpg'
    session['img'] = code_str.upper()
    return response



@passport_blu.route("/logout", methods=["GET", "POST"])
def logout():
    print("调用用户退出视图")
    # 清空session
    session.clear()

    # 返回信息
    ret = {
        "errno":0,
        "errmsg":"退出成功！"
    }

    return  jsonify(ret)


@passport_blu.route("/login", methods=["GET", "POST"])
def login():
    # {mobile: "13834065321", password: "123456"}
    # mobile: "13834065321"
    # password: "123456"
    mobile = request.json.get("mobile")
    password = request.json.get("password")
    user = db.session.query(User).filter(User.mobile == mobile ,User.password_hash == password).first()
    print(mobile, password, user)
    if user:
        ret = {
            "errno":0,
            "errmsg":"登录成功"
        }
        session["mobile"] = mobile
    else:
        ret = {
            "errno": 1001,
            "errmsg": "登录失败"
        }
    return jsonify(ret)



