from flask import Flask

from app.utils.common.common import rankings
from config import CONFIG
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()


def create_project(config_name):
    # 创建flask对象
    app = Flask(__name__)
    # 导入index下的蓝图
    from app.views.index import index_blu
    from app.views.passport import passport_blu
    from app.views.user import user_blu
    from app.views.news import news_blu
    from app.views.admin import admin_blu

    # 进行相关配置
    app.config.from_object(CONFIG.get(config_name))

    # 对蓝图进行注册
    app.register_blueprint(index_blu)
    app.register_blueprint(passport_blu, url_prefix="/passport")
    app.register_blueprint(user_blu, url_prefix="/user")
    app.register_blueprint(news_blu, url_prefix="/news")
    app.register_blueprint(admin_blu, url_prefix="/admin")


    # 对过滤器进行注册
    app.add_template_filter(rankings, "rankings")

    # 对app进行配置
    db.init_app(app)

    return app